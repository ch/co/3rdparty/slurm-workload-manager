-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: slurm-wlm
Binary: slurm-wlm, slurm-client, slurmrestd, slurmd, slurmctld, libslurm38, libpmi0, libpmi2-0, libslurm-dev, libpmi0-dev, libpmi2-0-dev, slurm-wlm-doc, slurm-wlm-basic-plugins, slurm-wlm-basic-plugins-dev, slurm-wlm-plugins, slurm-wlm-plugins-dev, slurm-wlm-ipmi-plugins, slurm-wlm-ipmi-plugins-dev, slurm-wlm-hdf5-plugin, slurm-wlm-hdf5-plugin-dev, slurm-wlm-rsmi-plugin, slurm-wlm-rsmi-plugin-dev, slurm-wlm-influxdb-plugin, slurm-wlm-influxdb-plugin-dev, slurm-wlm-rrd-plugin, slurm-wlm-rrd-plugin-dev, slurm-wlm-elasticsearch-plugin, slurm-wlm-elasticsearch-plugin-dev, slurm-wlm-jwt-plugin, slurm-wlm-jwt-plugin-dev, slurm-wlm-mysql-plugin, slurm-wlm-mysql-plugin-dev, sview, slurmdbd, libslurm-perl, libslurmdb-perl, slurm-wlm-torque, libpam-slurm, libpam-slurm-adopt, slurm-wlm-emulator, slurm-client-emulator
Architecture: any all
Version: 22.05.8-3ubuntu0.1
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders: Rémi Palancher <remi@rezib.org>, Mehdi Dogguy <mehdi@debian.org>, Gennaro Oliva <oliva.g@na.icar.cnr.it>,
Homepage: http://slurm.schedmd.com
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/hpc-team/slurm-wlm
Vcs-Git: https://salsa.debian.org/hpc-team/slurm-wlm.git
Testsuite: autopkgtest
Testsuite-Triggers: gcc, mariadb-client, mariadb-server, mpi-default-dev, mpich-doc
Build-Depends: debhelper (>= 11), libmunge-dev, libncurses-dev, po-debconf, python3, libgtk2.0-dev, default-libmysqlclient-dev, libpam0g-dev, libperl-dev, chrpath, liblua5.1-0-dev, libhwloc-dev, dh-exec, librrd-dev, libipmimonitoring-dev, hdf5-helpers, libfreeipmi-dev, libhdf5-dev, man2html, libcurl4-openssl-dev, libpmix-dev, libhttp-parser-dev, libyaml-dev, libjson-c-dev, libjwt-dev, liblz4-dev, bash-completion, libdbus-1-dev, librocm-smi-dev
Package-List:
 libpam-slurm deb admin optional arch=any
 libpam-slurm-adopt deb admin optional arch=any
 libpmi0 deb libs optional arch=any
 libpmi0-dev deb libdevel optional arch=any
 libpmi2-0 deb libs optional arch=any
 libpmi2-0-dev deb libdevel optional arch=any
 libslurm-dev deb libdevel optional arch=any
 libslurm-perl deb perl optional arch=any
 libslurm38 deb libs optional arch=any
 libslurmdb-perl deb perl optional arch=any
 slurm-client deb admin optional arch=any
 slurm-client-emulator deb admin optional arch=any
 slurm-wlm deb admin optional arch=any
 slurm-wlm-basic-plugins deb admin optional arch=any
 slurm-wlm-basic-plugins-dev deb devel optional arch=any
 slurm-wlm-doc deb doc optional arch=all
 slurm-wlm-elasticsearch-plugin deb admin optional arch=any
 slurm-wlm-elasticsearch-plugin-dev deb devel optional arch=any
 slurm-wlm-emulator deb admin optional arch=any
 slurm-wlm-hdf5-plugin deb admin optional arch=any
 slurm-wlm-hdf5-plugin-dev deb devel optional arch=any
 slurm-wlm-influxdb-plugin deb admin optional arch=any
 slurm-wlm-influxdb-plugin-dev deb devel optional arch=any
 slurm-wlm-ipmi-plugins deb admin optional arch=any
 slurm-wlm-ipmi-plugins-dev deb devel optional arch=any
 slurm-wlm-jwt-plugin deb admin optional arch=any
 slurm-wlm-jwt-plugin-dev deb devel optional arch=any
 slurm-wlm-mysql-plugin deb admin optional arch=any
 slurm-wlm-mysql-plugin-dev deb devel optional arch=any
 slurm-wlm-plugins deb admin optional arch=any
 slurm-wlm-plugins-dev deb admin optional arch=any
 slurm-wlm-rrd-plugin deb admin optional arch=any
 slurm-wlm-rrd-plugin-dev deb devel optional arch=any
 slurm-wlm-rsmi-plugin deb admin optional arch=any
 slurm-wlm-rsmi-plugin-dev deb devel optional arch=any
 slurm-wlm-torque deb admin optional arch=all
 slurmctld deb admin optional arch=any
 slurmd deb admin optional arch=any
 slurmdbd deb admin optional arch=any
 slurmrestd deb admin optional arch=any
 sview deb admin optional arch=any
Checksums-Sha1:
 0719f99008a22ee412b3729e1e9ae0440d841c07 9699785 slurm-wlm_22.05.8.orig.tar.gz
 25ec8319e36b8bc72f1f05911e26bc7a980608fa 134324 slurm-wlm_22.05.8-3ubuntu0.1.debian.tar.xz
Checksums-Sha256:
 8c8f6a26a5d51e6c63773f2e02653eb724540ee8b360125c8d7732314ce737d6 9699785 slurm-wlm_22.05.8.orig.tar.gz
 e77c4ad67f3af0e92624b708e3b881fa133772487dc3f73e3c92699e792b96b9 134324 slurm-wlm_22.05.8-3ubuntu0.1.debian.tar.xz
Files:
 f7c710747f8eac5fe1940d48ce9d2775 9699785 slurm-wlm_22.05.8.orig.tar.gz
 eca07a3c603ae5736e4646542b0da537 134324 slurm-wlm_22.05.8-3ubuntu0.1.debian.tar.xz
Original-Maintainer: Debian HPC Team <debian-hpc@lists.debian.org>

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEpmEQCz2sHU8srYpU5gOyV4+48PsFAmS3HcIACgkQ5gOyV4+4
8PsE6hAAh8KKgdA5YVQkOhC27jWcxgYuTjzaPPFUUc4Qzv0ye5Vp0PosbvWHtXLO
zGEGP1n3T3Q3/357f78JTU/SDwAAm7hWci5SEnfAeXR3rTdsKmNM2t9El4P28tRa
gOrdmn5oqS3pE36LxiFn7R7qkGbK+Ca8wmUZY6JXliuA0xR+5lZeEAOc4wXycKuJ
y1U5hjo3yLNU37koghIj0zStzupXEc+AL6KuXsC5mtKZN5o2zLDNQwnwwjkydJZ5
51VxZ2ZaDPaB7udQrLCGnltsnbYwC85UmJ5R6y2Ewx7EL/2h73bk4ac+RB7pj12w
IziYqbJKEu9OF71JAIAi8ucfuXxZeH0ih8m/g2rzVqAjSSHWL5BM2hxY/gnIkrBS
yAC7urT41e+oGFPavxOo7SnHH9tgviJxJIOlN1swAoIe0JCdFm+4lk8H8YizoT8J
CLg0NJTPMuhzrF4yrkqdcxKJx/f5RXslWnYQ+xBJ2B1hTIofLKBFcMC0dSIa5EaV
Knj0wSbCLtgmIbdKFGpdDTcLhESltHKmmdU2vh/QCTF7LlRBNHr4NJgJdp0CFWP3
kYNdYIBsr/rxXOBIEl35i2TWiyA+cWmfCk1fe4kKzv8MTAb4nCUzsOPJ4H+wQJjB
uCvxe9cW3eAROMF4FdS0/pHGWkV1Zt0EjJmBD6Xc0ehcPw01HV4=
=hTC/
-----END PGP SIGNATURE-----
